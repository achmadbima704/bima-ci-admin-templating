<?php

class Template
{
  
  public function __construct()
  {
    
    $this->_ci =& get_instance();
    

  }
  

  public function render($path, $data)
  {
    
    $data['_meta'] = $this->_ci->load->view('layout/_meta', $data, TRUE);
    $data['_stylesheet'] = $this->_ci->load->view('layout/_stylesheet.php', $data, TRUE);
    $data['_navigation'] = $this->_ci->load->view('layout/_navigation.php', $data, TRUE);
    $data['_script'] = $this->_ci->load->view('layout/_script.php', $data, TRUE);
    $data['_page'] = $this->_ci->load->view($path, $data, TRUE);
    
    $this->_ci->load->view('layout/_template', $data, FALSE);
    
  }

}
