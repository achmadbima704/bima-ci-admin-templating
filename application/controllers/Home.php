<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function index()
  {
    
    $data['title'] = 'Dashboard';

    $this->template->render('pages/dashboard', $data);

  }

}

/* End of file Home.php */
