<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Table extends CI_Controller {

  public function index()
  {

    $data['title'] = 'Table';
    
    $this->template->render('pages/table', $data);

  }

}

/* End of file Table.php */
