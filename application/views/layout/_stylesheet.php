<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?php echo base_url('assets/vendor/metisMenu/metisMenu.min.css') ?>" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo base_url('assets/dist/css/sb-admin-2.css') ?>" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="<?php echo base_url('assets/vendor/morrisjs/morris.css') ?>" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">