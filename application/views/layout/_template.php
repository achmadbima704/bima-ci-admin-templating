<!DOCTYPE html>
<html lang="en">
<head>
  <?php $_meta ?>
  <title> <?php echo $title ?> </title>
  <?php echo $_stylesheet ?>
</head>
<body>
  
  <div class="wrapper">
    
    <?php echo $_navigation ?>
    <?php echo $_page ?>

  </div>
  <?php echo $_script ?>
</body>
</html>